from setuptools import setup

setup(
    name= 'penguin',
    py_modules= ['penguin'],
    author= 'Yu Huo',
    author_email= 'yu.huo@ed.ac.uk',
    url= '',
    license='',
    description= 'For analysing Penguin turbidostat data.',
    python_requires= '>=3.6',
    include_package_data= True,
    install_requires= [
        'numpy',
        'matplotlib',
        'pandas',
        'seaborn',
        'gaussianprocess@git+https://git.ecdf.ed.ac.uk/pswain/gaussianprocess@master',
        ]
)
